xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.vatrox.com/osb/MonitorWS/JMSSender";
(:: import schema at "../../MonitorWS_JMSSender/Resources/JMSSender.xsd" ::)


declare variable $response as xs:string external;
declare variable $nivel as xs:string external;
declare variable $idTransaccion as xs:string external;
declare variable $operacion as xs:string external;
declare variable $mensaje as xs:string external;
declare variable $usuario as xs:string external;
declare variable $fechaInicio as xs:string external;
declare variable $estado as xs:string external;
declare variable $paso as xs:string external;
declare variable $request as xs:string external;
declare variable $sistema as xs:string external;


declare function local:func($response as xs:string,
                            $nivel as xs:string,
                            $idTransaccion as xs:string,
                            $operacion as xs:string,
                            $mensaje as xs:string,
                            $usuario as xs:string,
                            $fechaInicio as xs:string,
                            $estado as xs:string,
                            $paso as xs:string,
                            $request as xs:string,
                            $sistema as xs:string) as element() (:: schema-element(ns1:Message) ::) {
    <ns1:Message>
        <ns1:response>{fn:data($response)}</ns1:response>
        <ns1:producto>OSB</ns1:producto>
        <ns1:nivel>{fn:data($nivel)}</ns1:nivel>
        <ns1:idTransaccion>{fn:data($idTransaccion)}</ns1:idTransaccion>
        <ns1:servicio>Commons_ArchivosAdjuntos</ns1:servicio>
        <ns1:operacion>{fn:data($operacion)}</ns1:operacion>
        <ns1:mensaje>{fn:data($mensaje)}</ns1:mensaje>
        <ns1:usuario>{fn:data($usuario)}</ns1:usuario>
        {
          if (fn:string-length(fn:string($fechaInicio)) > 0) 
          then <ns1:fechaInicio>{fn:string($fechaInicio)}</ns1:fechaInicio>
          else(<ns1:fechaInicio/>)
        }
        <ns1:estado>{fn:data($estado)}</ns1:estado>
        <ns1:paso>{fn:data($paso)}</ns1:paso>
        <ns1:fechaFin>{fn:current-dateTime()}</ns1:fechaFin>
        <ns1:request>{fn:data($request)}</ns1:request>
        <ns1:version>v1</ns1:version>
        <ns1:sistema>{fn:data($sistema)}</ns1:sistema>
    </ns1:Message>
};

local:func($response, $nivel, $idTransaccion, $operacion, $mensaje, $usuario, $fechaInicio, $estado, $paso, $request, $sistema)

